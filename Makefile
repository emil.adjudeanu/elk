help: usage
start-all: start-elk-stack start-nginx
stop-all: stop-nginx stop-elk-stack
build-all: build-elk build-nginx

start-elk-stack:
	docker-compose -f docker-compose-elk.yml up -d --remove-orphans
start-nginx:
	docker-compose -f docker-compose-nginx.yml up -d
stop-elk-stack: stop-nginx
	docker-compose -f docker-compose-elk.yml down --remove-orphans
stop-nginx:
	docker-compose -f docker-compose-nginx.yml down --remove-orphans
build-elk:
	docker-compose -f docker-compose-elk.yml build
build-nginx:
	docker-compose -f docker-compose-nginx.yml build
usage:
	@echo "This runs 2 docker compose files: docker-compose-elk.yml and docker-compose-nginx.yml"
	@echo "You can run compose directly or use the sequence"
	@echo "make build-all - builds all the docker images"
	@echo "make start-all - brings up an elk stack and an nginx client"
	@echo "make stop-all - brings everything down"
	@echo "make start-elk-stack - starts the elk stack"
	@echo "make start-nginx - starts the nginx client - it needs the elk stack to be running"
	@echo "make stop-nginx - stops nginx"
	@echo "make stop-elk-stack - stops the elk stack"
