ELK stack using 2 nginx client containers. The project was created as a test platform for ELK and should be modified accordingly to your needs.

# Prerequisites:

On linux please setup the following:

```
sysctl -w vm.max_map_count=262144
```

File permissions required for es container (only runs as esuser account)
```
mkdir .data
chown -R 777 .data
```

Usage flags:
```
# make help
```
NOTE: Please ensure sufficient RAM is available for the docker elastic-search container - usually around 4GB.

# Monitor docker containers logs:
### Using filebeat
```
docker run --rm -d -v /var/run/docker.sock:/tmp/docker.sock --link logstash:logstash --network=elk_default --name filbeat-shipper -e LOGSTASH_HOST=logstash -e LOGSTASH_PORT=5000 -e SHIPPER_NAME=$(hostname) bargenson/filebeat
```
### Using metric beat (recommended)
Metricbeat container is defined in the docker-compose-elk stack and monitors the local docker engine.
```
make start-all
```

If you need to test custom logstash grok filters use this command while inside the logsstash docker container:
```
logstash-5.4.0/bin/logstash -f /tmp/logstash-elasticsearch-filter.conf --path.data /tmp > /tmp/junk
```

# Import Dashboards/Index Patterns:

NOTE: Change the beats dashboards release number to the desired one. Also for custom dashboards fork the official project or build your own.
```
# docker-compose -f docker-compose-elk.yml exec metricbeat /bin/bash
# /usr/share/metricbeat/scripts/import_dashboards -es http://es:9200 -url https://artifacts.elastic.co/downloads/beats/beats-dashboards/beats-dashboards-5.4.1.zip
```
